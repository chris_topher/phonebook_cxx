#include "Server.hpp"
#include <iostream>
#include <errno.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdexcept>
#include <algorithm>
#include <cstdio>
#include <cstring>
#include "OutputUtils.hpp"

Server::Server()
{
  this->port = 4242;
}

Server::~Server()
{
  //Todo: close everything
}

void  Server::setPort(int port)
{
  this->port = port;
}

void	Server::init()
{
  if (!this->initProtocolData())
    throw std::runtime_error("phonebook::server: Fatal error. Server is exiting.");
  if (!this->bindListen())
    throw std::runtime_error("phonebook::server: Fatal error. Server is exiting.");
  this->_clients.push_front(this->socket_fd);
  this->setFileDescriptors();
  std::cout << OutputUtils::okColor << "phonebook::server: Initialization complete."
	    << OutputUtils::initialColor << std::endl;
}

bool	Server::bindListen()
{
  if (bind(this->socket_fd, (struct sockaddr *)(&(this->s_in)),
	   sizeof(this->s_in)) == -1)
    {
      if (errno == EADDRINUSE)
	std::cerr << OutputUtils::errorColor << "phonebook::server: binding: Address already in use.\
\nPlease set another port than " << this->port << ", or wait a few seconds before relaunching the server." <<
	  OutputUtils::initialColor << std::endl;
      else
	{
	  perror((OutputUtils::errorColor + "phonebook::server: binding").c_str());
	  std::cerr << OutputUtils::initialColor;
	}
      close(this->socket_fd);
      return (false);
    }
  if (listen(this->socket_fd, 42) == -1)
    {
      perror((OutputUtils::errorColor + "phonebook::server: listening").c_str());
      std::cerr << OutputUtils::initialColor;
      if (close(this->socket_fd) == -1)
	return (false);
      return (false);
    }
  return true;
}

bool	Server::initProtocolData()
{
  this->s_in_size = sizeof(this->s_in_client);
  this->pe = getprotobyname("TCP");
  if (!(this->pe))
    return (false);
  if ((this->socket_fd = socket(AF_INET, SOCK_STREAM,
				this->pe->p_proto)) == -1)
    return (false);
  this->s_in.sin_family = AF_INET;
  this->s_in.sin_port = htons(this->port);
  this->s_in.sin_addr.s_addr = INADDR_ANY;
  return true;
}

bool	Server::setFileDescriptors()
{
  FD_ZERO(&(this->s_rd));
  for (int clientFileDescriptor : this->_clients)
    {
      FD_SET(clientFileDescriptor, &s_rd);
    }
  return true;
}

bool		Server::readForClient(int fileDescriptor, std::string &outputString)
{
  char		*buffer = new char[Server::defaultBufferSize + 1];

  std::memset(buffer, 0, Server::defaultBufferSize + 1);
  if (read(fileDescriptor, buffer, Server::defaultBufferSize) <= 0)
    {
      delete buffer;
      return false;
    }
  if (*buffer == 0)
    {
      delete buffer;
      return false;
    }
  outputString.assign(buffer);
  delete buffer;
  return true;
}

void		Server::removeClient(int fileDescriptor)
{
  auto		foundFdIterator = std::find(this->_clients.begin(),
					    this->_clients.end(), fileDescriptor);

  if (foundFdIterator != this->_clients.end())
    {
      FD_CLR(*foundFdIterator, &s_rd);
      close(*foundFdIterator);
      this->_clients.erase(foundFdIterator);
      std::cout << OutputUtils::infoColor << "phonebook::server: Client " << fileDescriptor << " exited." << OutputUtils::initialColor << std::endl;
    }
}

int			Server::getSpacesNumber(const std::string &command)
{
  auto stringIterator = std::find(command.begin(), command.end(), ' ');
  int	spacesNumber = 0;

  if (stringIterator == command.end())
    return 0;
  while (*stringIterator == ' ')
    {
      if (*stringIterator == ' ')
        ++spacesNumber;
      ++stringIterator;
    }
  return spacesNumber;
}

bool	Server::execute(const std::string &command, const std::string &parameter)
{
  bool commandResult = true;

  if (command == "sort")
    commandResult = _phonebook.sort(parameter);
  else if (command == "insert")
    commandResult =_phonebook.insert(parameter, true);
  else if (command == "delete")
    commandResult = _phonebook.deleteEntry(parameter);
  else
    {
      std::cerr << OutputUtils::errorColor << "phonebook::server: Bad query. Command \""
		<< command
		<< "\" not in list. Please read the usage."<< std::endl;
      return false;
    }
  if (commandResult == true)
    OutputUtils::dispOk("phonebook::server: Successfully executed command "
			+ command + ". Argument : " + parameter);
  return commandResult;
}

bool			Server::interpretCommand(const std::string &command)
{
  std::string::size_type commandDelimiterPosition;

  if (command.empty())
    return false;
  if (this->getSpacesNumber(command) == 1)
    commandDelimiterPosition = command.find(' ');
  else
  {
    OutputUtils::dispError("phonebook::server: Bad query. Incorrect number of spaces. 1 space ONLY between [COMMAND] and [ARGUMENT] is required by the protocol.");
    return false;
  }
  if (commandDelimiterPosition == std::string::npos)
    {
      std::cerr << OutputUtils::errorColor << "phonebook::server: Bad query \"" << command
		<< "\". Please read the usage."<< std::endl;
      return false;
    }
  std::string deducedCommand = command.substr(0, commandDelimiterPosition);
  std::string deducedParameter = command.substr(commandDelimiterPosition + 1);
  return this->execute(deducedCommand, deducedParameter);
}

bool		Server::readWhenSpeaking()
{
  auto		clientsDequeIterator = this->_clients.begin();
  int		firstElement = *clientsDequeIterator;
  std::string	clientOutput;

  if (firstElement == this->socket_fd)
    return true;
  while (clientsDequeIterator != this->_clients.end())
    {
      if (FD_ISSET(*clientsDequeIterator, &s_rd))
	{
	  if (this->readForClient(*clientsDequeIterator, clientOutput) == false &&
	      *clientsDequeIterator != this->socket_fd)
	    {
	      this->removeClient(*clientsDequeIterator);
	      return (false);
	    }
	  else
	    this->interpretCommand(clientOutput);
	}
      ++clientsDequeIterator;
    }
  return true;
}

void	Server::launch()
{
  int	err = 0;
  int	new_client_fd = this->socket_fd;

  std::cout << OutputUtils::okColor << "phonebook::server: Server launched.\
 Listening on localhost for port " << this->port << OutputUtils::initialColor << std::endl;
  while (err != -1)
    {
      this->setFileDescriptors();
      int maxFd = *std::max_element(this->_clients.begin(), this->_clients.end());
      err = select(maxFd + 1, &s_rd, NULL, NULL, NULL);
      if (FD_ISSET(this->socket_fd, &s_rd))
	{
	  new_client_fd = accept(this->socket_fd,
				 (struct sockaddr *)(&(this->s_in_client)),
				 &(this->s_in_size));
	  this->_clients.push_front(new_client_fd);
	  std::cout << OutputUtils::infoColor
		    << "phonebook::server: New client connected. File descriptor: "
		    << new_client_fd
		    << OutputUtils::initialColor
		    << std::endl;
	}
      this->readWhenSpeaking();
    }
}
