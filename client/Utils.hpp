#pragma once

#include <string>

namespace Utils
{

    constexpr unsigned int static obtainIntegerCode(const char* str, int h = 0)
    {
         return !str[h] ? 5381 : (obtainIntegerCode(str, h+1) * 33) ^ str[h];
    }

}