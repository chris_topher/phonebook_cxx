#pragma once

#include <iostream>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <cstdlib>
#include <stdio.h>
#include <unistd.h>
#include <cstring>
#include <string>
#include <strings.h>
#include <tuple>
#include <deque>
#include <map>
#define BUFF_SIZE 1024

#include "Arguments.hpp"
#include "MurphysLaw.hpp"

typedef bool (Arguments::*funcPtr)(std::string str);

class	Client
{
  protected:
    Arguments					arg;
    std::deque<std::string>			opt;
    std::map<std::string, funcPtr>		funcMap;
    struct protoent				*pr;
    int						socket_fd;
    struct sockaddr_in				s_in;
    int						port;
    char					*ip;
  public:
    //Constructor && destructor
    Client(char **av);
    ~Client();
    //Member functions
    bool	initializeObj();
    bool	isFromOptionsLexer(const std::string &str);
    bool	argsToClass(char **av);
    bool	initializeObj(char **av);
    bool	connectToSocket();
    bool	sendManuallyToServer();
    char	*readEntry();
    bool	mainLoopRecieve(); //Will probably contain a select
    char	*readFromServer();
    //Getters
    int		getSocketFd() const;
};
