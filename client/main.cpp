#include "Client.hpp"
#include "OutputUtils.hpp"

int		main(int , char **av)
{
  try
    {
      Client	client(av);
      if (client.initializeObj() == false)
      	return (false);
      if (client.connectToSocket() == false)
      	return (false);
      client.sendManuallyToServer();
      if (close(client.getSocketFd()) == -1)
      	return (false);
    }
  catch (const std::exception &e)
    {
      OutputUtils::dispError(e.what());
      return (false);
    }
  return (0);
}
