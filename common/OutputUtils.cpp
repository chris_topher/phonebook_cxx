#include "OutputUtils.hpp"
#include <iostream>

const std::string	OutputUtils::errorColor = "\033[1;31m";
const std::string	OutputUtils::okColor = "\033[1;32m";
const std::string	OutputUtils::infoColor = "\033[1;34m";
const std::string	OutputUtils::initialColor = "\033[0m";

void	OutputUtils::dispError(const std::string &errorMessage)
{
  std::cerr << OutputUtils::errorColor << errorMessage << OutputUtils::initialColor << std::endl;
}

void	OutputUtils::dispOk(const std::string &errorMessage)
{
  std::cout << OutputUtils::okColor << errorMessage << OutputUtils::initialColor << std::endl;
}

void	OutputUtils::dispInfo(const std::string &errorMessage)
{
  std::cout << OutputUtils::infoColor << errorMessage << OutputUtils::initialColor << std::endl;
}
