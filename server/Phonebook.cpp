#include <Phonebook.hpp>
#include <iostream>
#include <stdexcept>
#include <utility>
#include <cstdio>
#include <algorithm>
#include "OutputUtils.hpp"

Phonebook::Phonebook()
{
    _filestream.open(Phonebook::phonebookFilename, std::fstream::in | std::fstream::out | std::fstream::app);
    this->fillUp();
}

void    Phonebook::fillUp()
{
    std::string line;

    if (_filestream.is_open())
    {
        while (std::getline(_filestream, line))
        {
            if (line.empty())
                continue ;
            this->insert(line, false);
        }
        _filestream.flush();
        _filestream.clear();
    }
    else
        throw std::runtime_error("phonebook::Phonebook::fillUp: 'database.phonebook.d' file not provided. Read the usage.");
}

bool    Phonebook::insert(const std::string &keyValuePairCSV, bool inFileAsWell)
{
    std::string::size_type comaIndex;
    if ((comaIndex = keyValuePairCSV.find(';')) == std::string::npos)
      {
	OutputUtils::dispError("phonebook::Phonebook::insert: Badly formatted insert request. Read the usage.");
	return false;
      }
    std::string phoneNumberKey = keyValuePairCSV.substr(0, comaIndex);
    std::string nameValue = keyValuePairCSV.substr(comaIndex + 1);
    std::string outputString(phoneNumberKey + ';' + nameValue);
    this->_phonebook[phoneNumberKey] = nameValue;
    if (inFileAsWell == true)
    {
        this->_filestream << outputString << std::endl;
	OutputUtils::dispInfo("phonebook::Phonebook::insert: Successfully inserted phone number "
			      + phoneNumberKey + " for contact " +  nameValue + '.');
    }
    return true;
}

void    Phonebook::sortByNumbers()
{
    //A map orders its values by keys automatically. I just have to dump the content of the map.
    this->_filestream.close();
    this->_filestream.open(Phonebook::phonebookFilename,  std::fstream::trunc | std::fstream::out);
    for (std::pair<std::string, std::string> keyValue : this->_phonebook)
    {
        this->_filestream << keyValue.first << ';' << keyValue.second << std::endl;
    }
    this->_filestream.clear();
    this->_filestream.close();
    this->_filestream.open(Phonebook::phonebookFilename, std::fstream::in | std::fstream::out | std::fstream::app);
    OutputUtils::dispOk("phonebook::Phonebook:sort: Successfully sorted phonebook by numbers.");
}

void    Phonebook::dumpInFile(const std::vector<std::pair<std::string, std::string>> &toDump)
{
    this->_filestream.close();
    this->_filestream.open(Phonebook::phonebookFilename,  std::fstream::trunc | std::fstream::out);
    for (std::pair<std::string, std::string> keyValue : toDump)
    {
        this->_filestream << keyValue.first << ';' << keyValue.second << std::endl;
    }
    this->_filestream.clear();
    this->_filestream.close();
    this->_filestream.open(Phonebook::phonebookFilename, std::fstream::in | std::fstream::out | std::fstream::app);
}

void    Phonebook::sortByName()
{
    //Since the map is ordered by keys, I dump the values in a vector that I sort and then dump into the file.
    std::vector<std::pair<std::string, std::string>> phonebookDump;

    std::for_each(this->_phonebook.begin(), this->_phonebook.end(),
    [&phonebookDump](std::pair<std::string, std::string> keyValue) //c++11 lambdas
    {
        phonebookDump.push_back(keyValue);
    });

    std::sort(phonebookDump.begin(), phonebookDump.end(),
    [] (std::pair<std::string, std::string> lhs, std::pair<std::string, std::string> rhs)
    {
        return lhs.second < rhs.second;
    });
    this->dumpInFile(phonebookDump);
    OutputUtils::dispOk("phonebook::sort: Successfully sorted phonebook by name.");
}

bool    Phonebook::sort(const std::string &sortMethod)
{
    if (sortMethod == "numbers")
        this->sortByNumbers();
    else if (sortMethod == "name")
        this->sortByName();
    else
      {
	OutputUtils::dispError("phonebook::Phonebook::sort: Wrong sorting method argument. Read the usage.");
	return false;
      }
    return true;
}

bool    Phonebook::deleteEntry(const std::string &nameToDelete)
{
    auto foundMapIterator = std::find_if(this->_phonebook.begin(), this->_phonebook.end(),
    [nameToDelete] (std::pair<std::string, std::string> to_compare) //c++11 lambdas
    {
        return to_compare.second == nameToDelete;
    }
    );
    if (foundMapIterator != this->_phonebook.end())
    {
        this->_phonebook.erase(foundMapIterator);
        std::vector<std::pair<std::string, std::string>> toDump(this->_phonebook.begin(), this->_phonebook.end());
        this->dumpInFile(toDump);
	OutputUtils::dispOk("phonebook::Phonebook::delete: Successfully removed contact " + nameToDelete);
	return true;
    }
    else
      OutputUtils::dispError("phonebook::Phonebook::delete: " + nameToDelete + " not found.");
    return false;
}

Phonebook::~Phonebook()
{
    _filestream.close();
}
