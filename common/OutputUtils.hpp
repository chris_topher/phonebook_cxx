#pragma once
#include <string>

class OutputUtils
{
public:
  static const std::string	errorColor;
  static const std::string	okColor;
  static const std::string	infoColor;
  static const std::string	initialColor;

  static void	dispError(const std::string &errorMsg);
  static void	dispInfo(const std::string &errorMsg);
  static void	dispOk(const std::string &errorMsg);
};
