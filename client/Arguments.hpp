#pragma	once

#define DEFAULT_TEAM_NAME "FANTA"
#define DEFAULT_PORT 4242
#define DEFAULT_HOST "127.0.0.1"
#include <string>
#include "MurphysLaw.hpp"

class	Arguments
{
private:
  std::string	team_name;
  int		port;
  std::string	host;
public:
  Arguments()
  {
    this->team_name = DEFAULT_TEAM_NAME;
    this->port = DEFAULT_PORT;
    this->host = DEFAULT_HOST;
  }
  bool		setTeamName(std::string new_name);
  bool		setPort(std::string new_name);
  bool		setHost(std::string new_host);
  const	char	*getTeamName()
  {
    return this->team_name.c_str();
  }
  const char	*getHost()
  {
    return this->host.c_str();
  }
  int		getPort()
  {
    return this->port;
  }
};
