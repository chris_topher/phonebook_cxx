#include "Server.hpp"
#include "OutputUtils.hpp"
#include <iostream>
#include <stdexcept>

namespace ConverterHelper
{
  int   toInt(const std::string &new_port)
  {
    size_t	pos;
    int		the_port;

    try
      {
        the_port = std::stoi(new_port, &pos);
      }
    catch (std::invalid_argument &e)
      {
        throw std::invalid_argument("phonebook::client: Arguments error: Wrong argument '" +
		         new_port + "' for option -p");
      }
    if (new_port[pos] != 0 || the_port < 0)
      throw std::invalid_argument("phonebook::client: Arguments error: Wrong argument '" +
		       new_port + "' for option -p");
    return the_port;
  }  
}

int		main(int ac, char **av)
{
  try
    {
      Server	server;
      if (ac == 2)
        server.setPort(ConverterHelper::toInt(av[1]));
      server.init();
      server.launch();
      return (0);
    }
  catch (const std::exception &e)
    {
      std::cerr << OutputUtils::errorColor << e.what() << OutputUtils::initialColor << std::endl;
      return (1);
    }
}
