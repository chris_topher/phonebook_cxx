#include "MurphysLaw.hpp"

MurphysLaw::MurphysLaw(const std::string &msg)
{
  this->_msg = msg;
}

MurphysLaw::~MurphysLaw(void) throw ()
{}

const char *MurphysLaw::what(void) const throw ()
{
  return (this->_msg.c_str());
}
