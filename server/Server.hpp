#pragma once

#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <deque>
#include <string>
#include "Phonebook.hpp"

class Server
{
private:
  const	static size_t		defaultBufferSize = 1024;
  //Protocol data
  struct protoent       *pe;
  struct sockaddr_in    s_in;
  struct sockaddr_in    s_in_client;
  socklen_t             s_in_size;
  int                   socket_fd;
  int                   port;
  //Server data
  fd_set		s_rd;
  std::deque<int>	_clients;
  Phonebook		_phonebook;
  //Member functions
  bool			initProtocolData();
  bool			bindListen();
  bool			setFileDescriptors();
  bool			readWhenSpeaking();
  bool			readForClient(int fd, std::string &os);
  void			removeClient(int fd);
  bool			interpretCommand(const std::string &command);
  int			  getSpacesNumber(const std::string &param);
  bool			execute(const std::string &command, const std::string &parameter);
public:
  Server();
  ~Server();
  void	launch();
  void  setPort(int port);
  void			init();
};
