#pragma once
#include <vector>
#include <string>

class ArgumentParser
{
protected:
  std::vector<std::string> _commandLexer;
  std::string command;
  char **av;     
public:
  ArgumentParser(){}
  static void checkInput(const std::string &toto);
};
