#include "Client.hpp"
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <string>

namespace OutputHelper
{
  void	putchar(char c)
  {
    write(1, &c, 1);
  }
  
  void	printPrompt()
  {
    int		i;

    std::string	prompt("\e[1;34m(Enter command$)\e[1;0m");

    i = 0;
    while (prompt[i])
      {
	      OutputHelper::putchar(prompt[i]);
	      ++i;
      }
  }
}
char	*Client::readEntry()
{
  char	*buff;

  OutputHelper::printPrompt();
  if ((buff = static_cast<char *>(malloc(BUFF_SIZE * sizeof(char)))) == NULL)
    return (NULL);
  bzero(buff, BUFF_SIZE);
  if (read(1, buff, BUFF_SIZE) <= 0)
    return (NULL);
  if (buff[0] == 0)
    return (NULL);
  return (buff);
}

char	*Client::readFromServer()
{
  char	*buff;

  if ((buff = static_cast<char *>(malloc(BUFF_SIZE * sizeof(char)))) == NULL)
    return (NULL);
  bzero(buff, BUFF_SIZE);
  if (read(this->socket_fd, buff, BUFF_SIZE) <= 0)
    return (NULL);
  if (buff[0] == 0)
    return (NULL);
  return (buff);
}
