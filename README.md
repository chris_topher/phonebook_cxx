# What is this repository about ? #

This project is a multi-client TCP Server hand-coded with C and C++ standards.
It behaves like a phonebook, with a phone number database in CSV format.
No library dependency is present.
It supports Linux platform.
For a multiplatform mini-project, [go here](https://bitbucket.org/chris_topher/dynamiclibrariestp/src)

# Usage: #

```
./phonebook_server [PORT]
./phonebook_client [OPTION] [ARGUMENT]
```

When the client is launched, it is waiting for your command in its command line interface (CLI):

```
(Enter a command$)[COMMAND] [PARAMETER]
```
The server handles multi-client.
The phonebook file name must be `database.phonebook.d`.
We use CSV as data format, with the following formatting:
`[PhoneNumber];[ContactName]`

Availaible arguments for the client:

    -p:
        Set the port.
        Accepts any valid port number.
    -h:
        Set the host
        Accepts any valid IP address.

Availaible commands for the client's CLI:

    sort:
        Availaible arguments:
            numbers: Sort the phonebook file by phone numbers.
            name: Sort the phonebook file by name.
    insert:
        Availaible argument:
            [PhoneNumber];[ContactName]: Insert an entry for the contact [ContactName],
            having phone number [PhoneNumber].
    delete:
        Availaible argument:
        name: Delete all entries containing a given name.

# Getting started examples: #
```shell
make re
```
### On a first terminal: ###
```shell
./phonebook_server 4242
```
### On a second terminal: ###

```
./phonebook_client -p 4242
(Enter a command$)
```

## Examples: ##

### Sort by name: ###
    
```
(Enter a command$)sort name
```
    
### Insert a new contact: ###

```
(Enter a command$)insert 0169244030;Barry White
```
    
### Delete all the contacts named 'Barry White': ###
    
```
(Enter a command$)delete Barry White
```