#pragma once
#include <string>
#include <map>
#include <fstream>
#include <vector>

class Phonebook
{
    protected:
    std::fstream _filestream;
    std::map<std::string, std::string> _phonebook;
    void    fillUp();
    void    sortByNumbers();
    void    sortByName();
    void    dumpInFile(const std::vector<std::pair<std::string, std::string>> &toDump); 
    public:
        const std::string phonebookFilename = "./database.phonebook.d";
        Phonebook();
        ~Phonebook();
        bool    insert(const std::string &keyValuePairCSV, bool inFileAsWell);
        bool    deleteEntry(const std::string &nameToDelete);
        bool    sort(const std::string &sortMethod);
};
