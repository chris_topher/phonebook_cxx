#include "Client.hpp"
#include <errno.h>
#include <OutputUtils.hpp>

Client::Client(char **av)
{
  this->opt.push_back("-p");
  this->opt.push_back("-h");
  this->funcMap["-p"] = &Arguments::setPort;
  this->funcMap["-h"] = &Arguments::setHost;
  this->argsToClass(av);
}

Client::~Client()
{}

int		Client::getSocketFd() const
{
  return (this->socket_fd);
}

bool		Client::sendManuallyToServer()
{
  char		*buf;

  while ((buf = this->readEntry()) != NULL)
    {
      if (strcmp(buf, "quit\n") == 0)
      	return true;
      if (write(this->socket_fd, buf, strlen(buf) - 1) == -1)
	throw MurphysLaw(std::string("phonebook::client: client send error: ") + strerror(errno));
      free(buf);
    }
  return false;
}

bool		Client::mainLoopRecieve()
{
  fd_set	s_rd;
  short		err;
  char		*buf;

  err = 0;
  FD_ZERO(&s_rd);
  FD_SET(this->socket_fd, &s_rd);
  while (err != -1)
    {
      err = select(this->socket_fd + 1, &s_rd, NULL, NULL, NULL);
      if (FD_ISSET(this->socket_fd, &s_rd))
	{
	  if ((buf = this->readFromServer()) == NULL)
	    throw MurphysLaw("phonebook::client: client recieve error: Cannot read from server.");
	  printf("recieved %s\n", buf);
	}
      if (buf)
	free(buf);
    }
  return false;
}

bool		Client::connectToSocket()
{
  if (connect(this->socket_fd, (const struct sockaddr *)(&(this->s_in)),
	      sizeof(this->s_in)) == -1)
    {
      if (close(this->socket_fd) == -1)
        return (false);
      throw MurphysLaw(std::string("phonebook::client: client connect error: ") + strerror(errno));
    }
  OutputUtils::dispOk(std::string("phonebook::client: client initialized. Connected to address ")
  + const_cast<char *>(arg.getHost()) + ", on port " + std::to_string(arg.getPort()) + ".\n\
Welcome ! Please read the usage.txt for a list of commands.");
  return (true);
}

bool	Client::initializeObj()
{
  this->pr = getprotobyname("TCP");
  this->ip = const_cast<char *>(arg.getHost());
  this->port = arg.getPort();
  if (!(this->pr))
    throw MurphysLaw(std::string("phonebook::client: client initialization error: ") + strerror(errno));
  if ((this->socket_fd = socket(AF_INET, SOCK_STREAM,
				this->pr->p_proto)) == -1)
    throw MurphysLaw(std::string("phonebook::client: client initialization error: ") + strerror(errno));
  this->s_in.sin_family = AF_INET;
  this->s_in.sin_port = htons(this->port);
  this->s_in.sin_addr.s_addr = inet_addr(this->ip);
  return (true);
}

bool	Client::initializeObj(char **av)
{
  this->pr = getprotobyname("TCP");
  this->ip = av[1];
  this->port = atoi(av[2]);
  if (!(this->pr))
    throw MurphysLaw(std::string("phonebook::client: client initialization error: ") + strerror(errno));
  if ((this->socket_fd = socket(AF_INET, SOCK_STREAM,
				this->pr->p_proto)) == -1)
    throw MurphysLaw(std::string("phonebook::client: client initialization error: ") + strerror(errno));
  this->s_in.sin_family = AF_INET;
  this->s_in.sin_port = htons(this->port);
  this->s_in.sin_addr.s_addr = inet_addr(this->ip);
  return (true);
}
