NAMECLIENT	= phonebook_client

NAMESERVER	= phonebook_server

RM		= rm --force

SRCCLIENT		= client/ArgumentParser.cpp\
			client/Utils.cpp\
			client/Arguments.cpp\
			client/ClientArgsSet.cpp\
			client/ClientCommunication.cpp\
			client/Client.cpp\
			client/MurphysLaw.cpp\
			client/Utils.cpp\
			client/main.cpp\
			common/OutputUtils.cpp

SRCSERVER		= common/OutputUtils.cpp\
			server/Phonebook.cpp\
			server/Server.cpp\
			server/main.cpp

CC		= g++

OBJCLIENT		= $(SRCCLIENT:.cpp=.o)
OBJSERVER		= $(SRCSERVER:.cpp=.o)


CXXFLAGS += -W -g3 -std=c++11 -I. -I./client -I./server -I./common

all: $(NAMECLIENT) $(NAMESERVER)

$(NAMECLIENT): $(OBJCLIENT)
	$(CC) $(OBJCLIENT) -o $(NAMECLIENT)

$(NAMESERVER): $(OBJSERVER)
	$(CC) $(OBJSERVER) -o $(NAMESERVER)

clean:
	$(RM) $(OBJCLIENT) $(OBJSERVER)

fclean:		clean
	$(RM) $(NAMECLIENT) $(NAMESERVER)

re: fclean all

.PHONY: all fclean clean re
