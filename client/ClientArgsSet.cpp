#include "Client.hpp"
#include <functional>
#include <iostream>

bool	Client::isFromOptionsLexer(const std::string &str)
{
  std::deque<std::string>::iterator		it = this->opt.begin();

  while (it != this->opt.end())
    {
      if (*it == str)
	return true;
      ++it;
    }
  throw MurphysLaw("phonebook::client: No such option " + str);
  return false;
}

bool	Client::argsToClass(char **av)
{
  int		i;
  funcPtr	set;

  i = 1;
  while (av[i])
    {
      if (std::string(av[i]).find("-") != std::string::npos)
	if (isFromOptionsLexer(std::string(av[i])) == true)
	  {
	    set = funcMap[std::string(av[i])];
	    if (av[i + 1] == NULL || av[i + 1][0] == '-')
	      throw MurphysLaw(std::string("No argument for option ") + av[i]);
	    (this->arg.*set)(std::string(av[i + 1]));
	  }
      ++i;
    }
  return (true);
}
