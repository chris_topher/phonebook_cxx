#ifndef MURPHYSLAW_HPP_
# define MURPHYSLAW_HPP_

#include <stdexcept>
#include <string>

/*
** "Whatever can happen will happen" ~ Edward Murphy.
*/

class	MurphysLaw : public std::exception
{
  std::string		_msg;

public:
  MurphysLaw(const std::string &msg);
  virtual ~MurphysLaw(void) throw ();

  virtual const char *what() const throw ();
};

#endif /* !MURPHYSLAW_HPP_ */
