#include "Arguments.hpp"
#include <iostream>

bool	Arguments::setTeamName(std::string new_name)
{
  this->team_name = new_name;
  return (true);
}

bool		Arguments::setPort(std::string new_port)
{
  size_t	pos;
  int		the_port;

  try
    {
      the_port = std::stoi(new_port, &pos);
    }
  catch (std::invalid_argument &e)
    {
      throw MurphysLaw("phonebook::client: Arguments error: Wrong argument '" +
		       new_port + "' for option -p");
    }
  if (new_port[pos] != 0 || the_port < 0)
    throw MurphysLaw("phonebook::client: Arguments error: Wrong argument '" +
		     new_port + "' for option -p");
  this->port = the_port;
  return true;
}

bool	Arguments::setHost(std::string new_host)
{
  this->host = new_host;
  return true;
}
